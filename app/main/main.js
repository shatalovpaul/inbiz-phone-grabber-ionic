'use strict';

angular.module('olenLoyaltyApp')
  .config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/phone');
    $stateProvider
      .state('phone', {
        url: '/phone',
        templateUrl: 'main/templates/phone.html',
        controller: 'PhoneController',
        controllerAs: 'ctrl',
        cache: false
      })
      .state('receipt', {
        url: '/receipt/:phone',
        templateUrl: 'main/templates/receipt.html',
        controller: 'ReceiptController',
        controllerAs: 'ctrl',
        cache: false
      })
      .state('greetings', {
        url: '/greetings/:isError&:phone&:activeOffers',
        templateUrl: 'main/templates/greetings.html',
        controller: 'GreetingsCtrl',
        controllerAs: 'ctrl',
        cache: false
      });
  });
