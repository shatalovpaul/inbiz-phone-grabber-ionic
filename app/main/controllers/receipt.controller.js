'use strict';

angular.module('olenLoyaltyApp')
  .config(function (bcKeypadConfigProvider) {
    bcKeypadConfigProvider.setMaxLength(6);
  })
  .controller('ReceiptController',
    function ($scope, mainService, $log, $state) {
      var ctrl = this;

      ctrl.inputIsInvalid = false;

      ctrl.text = {};
      ctrl.text.send = 'Отправить';
      ctrl.text.callToEnterReceipt = 'Введите номер чека:';
      ctrl.text.skip = 'Пропустить';

      ctrl.receipt = '';

      ctrl.skip = function () {
        $state.go('greetings');
      };

      ctrl.submit = function () {

        ctrl.text.receiptIsInvalid = 'Пожалуйста, введите корректный номер';

        if (ctrl.receiptForm.$valid) {
          //return receiptResource.sendReceipt({}, angular.toJson(ctrl.phone),
          //  function (response) {
          //    //toReceiptState(response.id);
          //    $log.info(response);
          //  }, function (err) {
          //    $log.error('Something wrong with data sending:' + err);
          //  });
          $state.go('greetings');
        } else {
          ctrl.inputIsInvalid = true;
        }
      };
    });
