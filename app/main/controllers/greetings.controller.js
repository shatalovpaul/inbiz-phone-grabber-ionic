'use strict';

angular.module('olenLoyaltyApp')
  .controller('GreetingsCtrl',
    function ($scope, mainService, $log, $timeout, $interval, $state, $stateParams, CONFIG, offerResource) {
      var ctrl = this;
      var phone = $stateParams.phone ? $stateParams.phone : '';

      ctrl.isError = $stateParams.isError;
      ctrl.newOffersMode = false;

      ctrl.text = CONFIG.TEXT_SRC.GEETINGS;
      ctrl.offers = $stateParams.activeOffers ? angular.fromJson($stateParams.activeOffers) : '';

      function startToPhoneStateTimeout() {
        return $timeout(function () {
          ctrl.toPhoneState();
        }, CONFIG.SCREENS_CONFIG.GREETING_SCREEN_DELAY);
      }

      var toPhoneStateTimeout = startToPhoneStateTimeout();

      if (!ctrl.isError) {
        var checkForNewOffersInterval = $interval(checkForNewOffers, CONFIG.SCREENS_CONFIG.NOT_SHOWN_OFFERS_REQUEST_INTERVAL);
      }

      ctrl.toPhoneState = function () {
        $interval.cancel(checkForNewOffersInterval);
        $timeout.cancel(toPhoneStateTimeout);
        $state.go('phone');
      };

      function checkForNewOffers() {
        offerResource.getNewOffersForClientToShow({phone: phone},
          function (result) {
            console.log("Received result:" + result);
            if (!result._id) {
              return;
            }

            ctrl.newOffersMode = true;
            ctrl.offers = [];
            ctrl.offers[0] = result;
            $timeout.cancel(toPhoneStateTimeout);
            toPhoneStateTimeout = startToPhoneStateTimeout();
            $interval.cancel(checkForNewOffersInterval);

          }, function (err) {
            //TODO: Log error
          })
      }
    });
