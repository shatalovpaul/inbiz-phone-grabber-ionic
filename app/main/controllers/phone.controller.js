'use strict';

angular.module('olenLoyaltyApp')
  .config(function (bcKeypadConfigProvider) {
    bcKeypadConfigProvider.setMaxLength(10);
  })
  .controller('PhoneController',
    function ($scope, phoneResource, $log, $ionicModal, $state, CONFIG, statesHelper, $localForage) {
      var ctrl = this;

      ctrl.phone = '';
      ctrl.inputIsInvalid = false;
      ctrl.text = CONFIG.TEXT_SRC.PHONE;

      $scope.agreementsText = CONFIG.TEXT_SRC.AGREEMENTS_MODAL;
      $scope.back = $scope.agreementsText.back;
      $scope.$watch('ctrl.phone', function () {
        ctrl.inputIsInvalid = false;
      });

      var showReceiptScreen = CONFIG.SCREENS_CONFIG.SHOW_RECEIPT_SCREEN;

      var sendPhones = function (phones) {
        ctrl.inProgress = true;
        phoneResource.sendPhones({}, {
          'phone': phones,
          'pos': CONFIG.APP_CONFIG.POS.id},
          function (result) {
            ctrl.inProgress = false;
            statesHelper.toGreetingsState(null, ctrl.phone, angular.toJson(result.activeOffers));
          }, function (err) {
            ctrl.inProgress = false;
            statesHelper.toGreetingsState(true, ctrl.phone, "");
            $log.error('Something wrong with phone sending: ' + err);
          });
      };

      ctrl.submit = function () {
        if (ctrl.phoneForm.$valid) {
          $localForage.getItem('phones').then(
            function (phonesArray) {

              if (!phonesArray) {
                phonesArray = [];
              }

              phonesArray.push(ctrl.phone);

              return $localForage.setItem('phones', phonesArray);
            }
          ).then(
            function () {
              sendPhones(ctrl.phone);
            }
          );

        } else {
          ctrl.inputIsInvalid = true;
        }
      };

      ctrl.openAgreement = function () {
        $scope.openAgreementModal();
      };

      $ionicModal.fromTemplateUrl('main/templates/agreements-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
      });

      $scope.openAgreementModal = function () {
        $scope.modal.show();
      };

      $scope.closeModal = function () {
        $scope.modal.hide();
      };

      $scope.$on('$destroy', function () {
        $scope.modal.remove();
      });
    });
