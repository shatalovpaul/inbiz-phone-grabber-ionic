'use strict';

angular.module('olenLoyaltyApp')
  .service('statesHelper', function ($state) {
    var service = this;


    //TODO:make parent state shange layer with transmitting of parameter
    service.toReceiptState = function () {
      $state.go('receipt');
    };

    service.toGreetingsState = function (isError, phone, activeOffers) {
      $state.go('greetings', {
        isError: isError,
        activeOffers: activeOffers,
        phone: phone
      });
    };
  });
