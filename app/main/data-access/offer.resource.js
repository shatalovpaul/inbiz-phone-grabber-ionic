'use strict';

angular.module('olenLoyaltyApp')
  .factory('offerResource', function ($resource, HTTP_CONFIG) {
    return $resource(HTTP_CONFIG.ENDPOINT + '', {}, {
      getNewOffersForClientToShow: {
        method: 'GET',
        url: HTTP_CONFIG.ENDPOINT + '/api/offers/getNotShownOffers/:phone',
        headers: {
          'Content-Type': 'application/json',
          'x-auth': "foamily87764'hepatica"
        }
      }
    });
  });
