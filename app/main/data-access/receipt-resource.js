'use strict';

angular.module('olenLoyaltyApp')
  .factory('receiptResource', function ($resource, CONFIG) {
    return $resource(CONFIG.HTTP_CONFIG.ENDPOINT + '', {}, {
      sendReceipt: {
        method: 'POST',
        url: CONFIG.HTTP_CONFIG.ENDPOINT + '/receipt',
        headers: {
          'Content-Type': 'application/json'
        }
      }
    });
  });
