'use strict';

angular.module('olenLoyaltyApp')
  .constant('HTTP_CONFIG', {
    /*
     Dev environment
     */
    //'ENDPOINT': 'http://localhost:9000'

    /*
     Test environment
     */
    //'ENDPOINT': 'http://217.182.88.221:81'

    /*
     Production environment
     */
    'ENDPOINT': 'http://olenvrn.dd-loyalty.com'
  });
