'use strict';

angular.module('olenLoyaltyApp')
  .constant('CONFIG', {
    'APP_CONFIG' : {
      'POS' : {
        //id: 'grd'
        id: 'cntr'
      }
    },
    'SCREENS_CONFIG': {
      'SHOW_RECEIPT_SCREEN': false,
      'GREETING_SCREEN_DELAY': 30000,
      'NOT_SHOWN_OFFERS_REQUEST_INTERVAL': 2000
    },
    'TEXT_SRC': {
      'PHONE': {
        registerCall: 'Зарегистрировать*',
        iAccept: '*регистрируя номер телефона, я даю своё согласие на обработку',
        processing: 'моих персональных данных в соответствиями с',
        conditions: 'условиями',
        callToEnterPhone: 'Введите номер телефона:',
        phoneIsInvalid: 'Пожалуйста, введите корректный номер'
      },
      'GEETINGS': {
        greetings: 'Спасибо за визит!',
        instructions: 'Выберите один из доступных бонусов:',
        instructionsNewOffers: 'Ваше следующее предложение:',
        instructionsNoOffers: 'К сожалению, у Вас нет доступных бонусов',
        ok: 'Понятно',
        error: 'К сожалению, сейчас нет связи со спутникам...',
        errorInstructinos: 'Может быть Вам повезет в следующий раз! :)'
      },
      'RECEIPT': {},
      'AGREEMENTS_MODAL': {
        title: 'Пользовательское соглашение',
        ok: 'Все ясно',
        back: 'Назад'
      }
    }
  });
