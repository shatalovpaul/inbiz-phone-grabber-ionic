'use strict';
angular.module('olenLoyaltyApp', [
    'ionic',
    'ngCordova',
    'ui.router',
    'ngResource',
    'ui.mask',
    'bc.AngularKeypad',
    'LocalForageModule'
  ]);
